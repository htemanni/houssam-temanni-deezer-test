package com.deezer.exoapplication.viewmodel

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer

class MainViewModel : ViewModel() {
    lateinit var player: SimpleExoPlayer
    var playerState = MutableLiveData(PlayerState.IDLE)

    internal fun setup(context: Context) {
        if (!::player.isInitialized) {
            player = SimpleExoPlayer.Builder(context).build()
            player.addListener(PlayerEventListener(playerState))
        }
    }


    fun playTrack(url: String) {
        playerState.value = PlayerState.PLAYING
        setUrl(url)
    }

    fun stopPlayer() {
        player.stop()
        player.clearMediaItems()
    }

    internal fun setUrl(url: String) {
        val media = MediaItem.fromUri(url)
        setMedia(media)
    }

    internal fun setMedia(media: MediaItem) {
        player.setMediaItem(media)
        player.prepare()
    }

    private class PlayerEventListener(val playerState: MutableLiveData<PlayerState>) :
        Player.EventListener {
        override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
            if (playbackState == Player.STATE_ENDED) {
                playerState.value = PlayerState.FINISHED
            }
        }
    }
}


enum class PlayerState() {
    IDLE,
    PLAYING,
    FINISHED
}