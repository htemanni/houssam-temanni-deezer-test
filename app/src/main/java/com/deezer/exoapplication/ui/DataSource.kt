package com.deezer.exoapplication.ui

import com.deezer.exoapplication.Model.Track
import java.util.ArrayList

fun getTrackList(): ArrayList<Track> {
    val trackList = ArrayList<Track>()
    trackList.addAll(
        listOf(
            Track(
                "Track 1",
                "https://filesamples.com/samples/audio/mp3/sample1.mp3"
            ),
            Track(
                "Track 2",
                "https://filesamples.com/samples/audio/mp3/sample2.mp3"
            ),
            Track(
                "Track 3",
                "https://filesamples.com/samples/audio/mp3/sample3.mp3"
            ),
            Track(
                "Track 4",
                "https://filesamples.com/samples/audio/mp3/sample4.mp3"
            )
        )
    )

    return trackList
}