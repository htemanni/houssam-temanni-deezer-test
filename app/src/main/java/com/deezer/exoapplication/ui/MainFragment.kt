package com.deezer.exoapplication.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.deezer.exoapplication.databinding.MainFragmentBinding
import com.deezer.exoapplication.Adapter.TrackAdapter
import com.deezer.exoapplication.viewmodel.MainViewModel
import com.deezer.exoapplication.Model.Track
import com.deezer.exoapplication.viewmodel.PlayerState
import java.util.*

class MainFragment : Fragment() {

    private lateinit var viewBinding: MainFragmentBinding
    private val viewModel: MainViewModel by activityViewModels()

    lateinit var trackList: ArrayList<Track>
    private lateinit var mAdapter: TrackAdapter
    private var currentIndex = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        trackList = getTrackList()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = MainFragmentBinding.inflate(inflater)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewBinding.main.setOnClickListener { viewBinding.playerControlView.show() }
        viewModel.setup(requireContext().applicationContext)
        viewBinding.playerControlView.player = viewModel.player
        setupRecyclerView()
        observePlayerState()

    }

    private fun setupRecyclerView() {
        mAdapter = TrackAdapter(requireContext().applicationContext, trackList, object :
            TrackAdapter.RecyclerItemClickListener {
            override fun onClickListener(track: Track?, position: Int) {
                changeSelectedTrack(position)
            }

            override fun onDeleteClickListener(position: Int) {
                deleteTrack(position)
            }
        }
        )

        viewBinding.recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(
                DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
            )
            adapter = mAdapter
        }
        mAdapter.notifyDataSetChanged()
    }

    private fun observePlayerState() {
        viewModel.playerState.observe(viewLifecycleOwner, {
            if (trackList.isNotEmpty()) {
                when (it) {
                    PlayerState.IDLE -> {
                        playTack(0)
                    }
                    PlayerState.FINISHED -> {
                        moveToNext()
                    }
                }
            }
        })
    }

    private fun moveToNext() {
        currentIndex = computeNextCurrentIndex()
        changeSelectedTrack(currentIndex)
    }

    private fun playTack(index: Int) {
        if ((trackList.isNotEmpty()) && (index <= trackList.size - 1)) {
            viewModel.playTrack(trackList[index].urlStream)
        }
    }

    private fun computeNextCurrentIndex() = if (currentIndex < trackList.size - 1) {
        currentIndex + 1
    } else 0


    fun deleteTrack(position: Int) {
        if (position == mAdapter.selectedPosition) {
            viewModel.stopPlayer()
            playTack(mAdapter.selectedPosition)
        }
        mAdapter.deleteItem(position)
    }

    private fun changeSelectedTrack(index: Int) {
        currentIndex = index
        mAdapter.apply {
            notifyItemChanged(selectedPosition)
            selectedPosition = currentIndex
            notifyItemChanged(currentIndex)
        }
        playTack(currentIndex)
    }


    override fun onDestroy() {
        super.onDestroy()
        viewBinding.playerControlView.player = null
    }

    companion object {
        fun newInstance() = MainFragment()
    }
}
