package com.deezer.exoapplication.Model

 data class Track (val title: String, val urlStream: String)