package com.deezer.exoapplication.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.deezer.exoapplication.R
import com.deezer.exoapplication.Adapter.TrackAdapter.TrackViewHolder
import com.deezer.exoapplication.Model.Track
import java.util.*

class TrackAdapter(
    private val context: Context,
    private val trackList: ArrayList<Track>,
    private val listener: RecyclerItemClickListener
) : RecyclerView.Adapter<TrackViewHolder>() {

    var selectedPosition = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrackViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.track_row, parent, false)
        return TrackViewHolder(view)
    }

    override fun onBindViewHolder(holder: TrackViewHolder, position: Int) {
        val track = trackList[position]
        if (track != null) {
            if (selectedPosition == position) {
                holder.itemView.setBackgroundColor(
                    ContextCompat.getColor(context, R.color.design_default_color_primary)
                )
            } else {
                holder.itemView.setBackgroundColor(
                    ContextCompat.getColor(
                        context,
                        android.R.color.transparent
                    )
                )
            }
            holder.bind(track, listener)
        }
    }

    override fun getItemCount(): Int {
        return trackList.size
    }

    class TrackViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val title: TextView = itemView.findViewById<View>(R.id.title) as TextView
        private val deleteButton: View = itemView.findViewById(R.id.delete_button)

        fun bind(
            track: Track,
            listener: RecyclerItemClickListener
        ) {
            title.text = track.title
            itemView.setOnClickListener {
                listener.onClickListener(
                    track,
                    layoutPosition
                )
            }
            deleteButton.setOnClickListener {
                listener.onDeleteClickListener(layoutPosition)
            }
        }
    }

    fun deleteItem(index: Int) {
        // update selected position if we delete the last and selected track (for list having more than one item)
        if ((index == selectedPosition) && (index == trackList.size - 1) && (selectedPosition != 0)) {
            selectedPosition -= 1
        }
        trackList.removeAt(index)
        notifyDataSetChanged()
    }

    interface RecyclerItemClickListener {
        fun onClickListener(track: Track?, position: Int)
        fun onDeleteClickListener(position: Int)
    }
}